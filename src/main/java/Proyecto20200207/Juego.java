/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto20200207;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author student
 */
public class Juego {
    private int[][] MatrizOriginal;
    private int[][] Matriz;
    private int[] PosYX = new int[2]; //posicion cero
    private ArrayList<Posicion> listaMovimientos = new ArrayList<>();
    private boolean TieneCero = false;
    
    
    
    public Juego (int[][] matriz){
        this.MatrizOriginal = matriz;
        this.Matriz = matriz;
    }
    public int[][] getMatriz(){
        return this.Matriz;
    } 
    public int[][] getMatrizOriginal(){
        return this.MatrizOriginal;
    } 
    public int[] getPosCero(){
        return this.PosYX;
    }
    public boolean getBanderaCero(){
        return this.TieneCero;
    }
    
    public boolean verificarTamanho(){
        boolean matrizCuadrada = true;
        for(int i=0; i<this.Matriz.length; i++){
            if(this.Matriz.length%this.Matriz[i].length != 0)//Asi verificamos que todas las filas
                matrizCuadrada=false;                       //Correspondan a las columnas en tama�o
        }
        return matrizCuadrada;
    }
    public void verificarTieneCero(){
        boolean respuesta = false;
        int contador = 0;
        for(int i=0; i<this.Matriz.length; i++){
            for (int j=0; j<this.Matriz[i].length; j++){
                if(this.Matriz[i][j]==0){
                    this.PosYX[0] = i;//Aprovechando la verificacion del cero
                    this.PosYX[1] = j;//Podemos guardar las coordenadas donde se encuentra
                    respuesta = true;
                    contador++;
                }
            }
        }
        if(contador!=1)
            respuesta = false;
        this.TieneCero = respuesta;
    }
    public String imprimirTablero(){
        String tableroImpreso="";
        for(int i=0; i<this.Matriz.length; i++){
            for(int j=0; j<this.Matriz[i].length; j++){
            tableroImpreso=tableroImpreso+ this.Matriz[i][j]+ ",\t";
            }
            tableroImpreso=tableroImpreso + "\n";
        }
        return tableroImpreso;
    }

    public ArrayList<Posicion> listaPosiblesMovidas() {
        ArrayList<Posicion> listaMovidas = new ArrayList<>();
        int x=this.PosYX[1];
        int y=this.PosYX[0];
        if(y-1>=0)//movimiento hacia arriba
            listaMovidas.add(new Posicion(y-1,x));
        if(x-1>=0)//movimiento hacia izquierda
            listaMovidas.add(new Posicion(y, x-1));
        if(x+1<this.Matriz.length)//movimiento hacia derecha
            listaMovidas.add(new Posicion(y, x+1));
        if(y+1<this.Matriz.length)//movimiento hacia abajo
            listaMovidas.add(new Posicion(y+1, x));
        return listaMovidas;
    }

    public void mover(Posicion posicion) {
        int yInicial=this.PosYX[0];
        int xInicial=this.PosYX[1];
        int yFinal=posicion.getPosY();
        int xFinal=posicion.getPosX();
        this.Matriz[yInicial][xInicial]=this.Matriz[yFinal][xFinal];
        this.Matriz[yFinal][xFinal]=0;
        this.PosYX[0] = yFinal;//Actualizamos la posicion del cero
        this.PosYX[1] = xFinal;
        
    }

    public void desarmarTablero(int numeroMovidas) {
        
        ArrayList<Posicion> listaAuxiliarMovidas= new ArrayList<>();//Lista de las posibles movidas del cero en cada ocasion.
        Random r = new Random();
        int i = 0;
        while(i<numeroMovidas){
            listaAuxiliarMovidas = this.listaPosiblesMovidas();
            int cantidadMovidas= listaAuxiliarMovidas.size();
            int seleccionAleatoria = r.nextInt(cantidadMovidas);//obtener un indice aleatorio
            this.mover(listaAuxiliarMovidas.get(seleccionAleatoria));
            this.listaMovimientos.add(listaAuxiliarMovidas.get(seleccionAleatoria));
            i++;
        }
    }
    public void moverArriba(){
        int x=this.PosYX[1];
        int y=this.PosYX[0];
        if(y-1>=0){//limite movimiento hacia arriba
            Posicion pos = new Posicion(y-1, x);
            this.mover(pos);
            this.listaMovimientos.add(pos);
        }
    }
    public void moverAbajo(){
        int x=this.PosYX[1];
        int y=this.PosYX[0];
        if(y+1<this.Matriz.length){//limite movimiento hacia abajo
            Posicion pos = new Posicion(y+1,x);
            this.mover(pos);
            this.listaMovimientos.add(pos);
        }
    }
    public void moverIzquierda(){
        int x=this.PosYX[1];
        int y=this.PosYX[0];
        if(x-1>=0){//limite movimiento hacia izquierda
            Posicion pos = new Posicion(y, x-1);
            this.mover(pos);
            this.listaMovimientos.add(pos);
        }
    }
    public void moverDerecha(){
        int x=this.PosYX[1];
        int y=this.PosYX[0];
        if(x+1<this.Matriz.length){//limite movimiento hacia derecha
            Posicion pos = new Posicion(y, x+1);
            this.mover(pos);
            this.listaMovimientos.add(pos);
        }
    }

    public void rearmarJuego() {
        for(int i=this.listaMovimientos.size()-1; i>=0; i--){
            this.mover(this.listaMovimientos.get(i));
        }
    }
}
