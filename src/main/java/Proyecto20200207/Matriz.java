/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto20200207;

/**
 *
 * @author student
 */
public class Matriz {
    private int[][] Matriz;
    
    public Matriz(double numeroFichas){
        int columnas = (int)Math.sqrt(numeroFichas);
        int filas = (int) ((int)(numeroFichas/columnas)+(numeroFichas%columnas));//de modo que pueda darse el caso de que no sea una matriz cuadrada
        int [][] matriz = new int[filas][columnas];
        for(int i=0;i<filas;i++){
            for(int j=0; j<columnas; j++){
                matriz[i][j]=(int) (Math.random() * 10);
            }
        }
        this.Matriz=matriz;
    }
    public int[][] getMatriz(){
        return this.Matriz;
    } 
}
