/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto20200207;

/**
 *
 * @author aleja
 */
public class Posicion {
    private int [] PosYX = new int[2];
    
    public Posicion(int posY, int posX){//Filas / columnas
        this.PosYX[1]=posX;
        this.PosYX[0]=posY;
    }
    public int[] getPos(){
        return this.PosYX;
    }
    public void setPosX(int posX){
        this.PosYX[1]=posX;
    }
    public void setPosY(int posY){
        this.PosYX[0]=posY;
    }
    public int getPosX(){
        return this.PosYX[1];
    }
    public int getPosY(){
        return this.PosYX[0] ;
    }
}
