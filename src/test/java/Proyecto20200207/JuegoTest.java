/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyecto20200207;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author student
 */
public class JuegoTest {
    
    public JuegoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   @Test
   public void testRompecabezasEsMatrizCuadrada(){
       int[][] matriz = new int[][]{
               {1,2,3,4},
               {5,6,7,8},
               {9,10,11,12},
               {13,14,15,0}
       };
       Juego juego = new Juego(matriz);
       boolean respuesta = juego.verificarTamanho();
       assertEquals(respuesta, true);
    }
   @Test
   public void testRompecabezasNoEsMatrizCuadrada(){
       int[][] matriz = new int[][]{
               {1,2,3,4},
               {5,6,7,8},
               {9,10,11,12}};
       Juego juego = new Juego(matriz);
       boolean respuesta = juego.verificarTamanho();
       assertEquals(respuesta, false);
    }
   @Test
   public void testVerificarTieneCero(){
       int[][] matriz = new int[][]{
               {1,2,3,4},
               {5,6,7,8},
               {9,10,11,12},
               {13,14,15,0}
       };
       Juego juego = new Juego(matriz);
       juego.verificarTieneCero();
       boolean respuesta = juego.getBanderaCero();
       assertEquals(respuesta, true);
   }
   @Test
   public void testVerificarTieneCeroCuandoNoHayCero(){
       int[][] matriz = new int[][]{
               {1,2,3,4},
               {5,6,7,8},
               {9,10,11,12},
               {13,14,15,16}
       };
       Juego juego = new Juego(matriz);
       juego.verificarTieneCero();
       boolean respuesta = juego.getBanderaCero();
       assertEquals(respuesta, false);
   }
    public void testVerificarTieneCeroCuandoHayMasDeUnCero(){
       int[][] matriz = new int[][]{
               {1,2,3,4},
               {5,0,7,8},
               {9,10,11,12},
               {13,14,0,16}
       };
       Juego juego = new Juego(matriz);
       juego.verificarTieneCero();
       boolean respuesta = juego.getBanderaCero();
       assertEquals(respuesta, false);
   }
@Test 
public void testTableroImpreso(){
        int[][] matriz = new int[][]{
               {1,2,3,4},
               {5,6,7,8},
               {9,10,11,12},
               {13,14,15,0}
       };
        Juego juego = new Juego(matriz);
        String respuesta = juego.imprimirTablero();
        String respuestaEsperada = "1,\t2,\t3,\t4,\t\n"
                + "5,\t6,\t7,\t8,\t\n"
                + "9,\t10,\t11,\t12,\t\n"
                + "13,\t14,\t15,\t0,\t\n";
        assertEquals(respuesta, respuestaEsperada);
    }
    @Test
    public void testPosicionCero(){
        int[][] matriz = new int[][]{
               {1,2,3,4},
               {5,6,7,8},
               {9,10,11,12},
               {13,14,15,0}
       };
        Juego juego = new Juego(matriz);
        juego.verificarTieneCero();//Guardamos la posicion del cero
        int[] resultado = juego.getPosCero();
        int[] resultadoEsperado = new int[] {3,3};
        Assert.assertArrayEquals(resultado, resultadoEsperado);
    }
   @Test
   public void testPosiblesMovidas(){
       int [][] matriz = new int[][]{
                    {1,0,3,4},
                    {5,6,7,8},
                    {9,10,11,12},
                    {13,14,15,16}};
       Juego juego = new Juego(matriz);
       juego.verificarTieneCero();//Guardamos la posicion del cero
       ArrayList<Posicion> posiblesMovidas = new ArrayList<>();
       posiblesMovidas = juego.listaPosiblesMovidas();
       Posicion pos1= new Posicion(0, 0);// (Columna,Fila)
       Posicion pos2= new Posicion(0, 2);
       Posicion pos3= new Posicion(1, 1);
       assertArrayEquals(posiblesMovidas.get(0).getPos(), pos1.getPos());
       assertArrayEquals(posiblesMovidas.get(1).getPos(), pos2.getPos());
       assertArrayEquals(posiblesMovidas.get(2).getPos(), pos3.getPos());
       }
       @Test
       public void testAplicarMovida(){
           int [][] matriz = new int[][]{
                    {1,0,3,4},
                    {5,6,7,8},
                    {9,10,11,12},
                    {13,14,15,16}};
       Juego juego = new Juego(matriz);
       juego.verificarTieneCero(); //Guardamos la posicion del cero
       ArrayList<Posicion> posiblesMovidas = new ArrayList<>();
       posiblesMovidas = juego.listaPosiblesMovidas();
       int [][] matrizResultante = new int[][]{
                    {0,1,3,4},
                    {5,6,7,8},
                    {9,10,11,12},
                    {13,14,15,16}};
        juego.mover(posiblesMovidas.get(0));//usando el anterior resultado, movemos a la izquierda
        assertArrayEquals(juego.getMatriz(), matrizResultante);
       }
       @Test
       public void testDesarmarTablero(){
            int [][] matriz = new int[][]{
                    {1,0,3,4},
                    {5,6,7,8},
                    {9,10,11,12},
                    {13,14,15,16}};
       Juego juego = new Juego(matriz);
       int[] posCeroOriginal = new int[] {0,1};
       juego.verificarTieneCero();//Guardamos la Posicion del Cero
       //La lista de movimientos es un atributo de mi clase Juego.
        juego.desarmarTablero(5);//La posicion del cero es un atributo de mi clase juego
        assertNotEquals(posCeroOriginal, juego.getPosCero());//Al ser la posicion original del cero
        //Distinta de la posicion final, se puede determinar que el tablero ha sido desordenado
           
       }
       @Test
       public void testJugabilidad(){
           int [][] matriz = new int[][]{
                    {1,0,3,4},
                    {5,6,7,8},
                    {9,10,11,12},
                    {13,14,15,16}};
       Juego juego = new Juego(matriz);
       juego.verificarTieneCero();//Guardamos la Posicion del Cero
       juego.moverAbajo();
       juego.moverDerecha();
       juego.moverAbajo();
       int[][] matrizResultanteEsperada = new int[][]{
                    {1,6,3,4},
                    {5,7,11,8},
                    {9,10,0,12},
                    {13,14,15,16}};
       
           Assert.assertArrayEquals(juego.getMatriz(), matrizResultanteEsperada);
       }
       @Test
       public void testReArmarTablero(){
           int [][] matriz = new int[][]{
                    {1,0,3,4},
                    {5,6,7,8},
                    {9,10,11,12},
                    {13,14,15,16}};
       Juego juego = new Juego(matriz);
       juego.verificarTieneCero();//Guardamos la Posicion del Cero
       int[] posCeroOriginal = juego.getPosCero();
       juego.desarmarTablero(5);
       juego.moverAbajo();
       juego.moverDerecha();
       juego.moverAbajo();
       juego.rearmarJuego();
       Assert.assertArrayEquals(posCeroOriginal, juego.getPosCero());
       }
       
      
   }
